import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoryComponent } from './history/history.component';
import { CreateComponent } from './create/create.component';
import { ReceivedComponent } from './received/received.component';
import { QuestionFinishComponent } from './question-finish/question-finish.component';
import { ScoreComponent } from './score/score.component';

const routes: Routes = [
  { path: 'history', component: HistoryComponent },
  { path: 'create', component: CreateComponent },
  { path: 'received', component: ReceivedComponent },
  { path: 'question-finish', component: QuestionFinishComponent },
  { path: 'score', component: ScoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
