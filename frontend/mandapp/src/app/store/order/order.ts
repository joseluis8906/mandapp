import { Event } from './event'

export class Order {
  id: string
  customerId: string
  courierId: string
  events: Event[]
  amount: number
  score: number

  constructor() {
    this.id = ""
    this.customerId = ""
    this.courierId = ""
    this.events = []
    this.amount = 0.0
    this.score = 0
  }
}