export class Event {
  number: number
  address: string
  description: string

  constructor(number_: number, address: string, description: string) {
    this.number = number_
    this.address = address
    this.description = description
  }
}