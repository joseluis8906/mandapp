import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Order } from '../store/order/order';
import { Observable } from 'rxjs';
import { addEvent, delEvent, addOrder } from '../store/order/order.actions';
import { Event } from '../store/order/event';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  router: Router
  private store: Store<{ order: Order }>
  order$: Observable<Order>
  formBuilder: FormBuilder
  form: FormGroup

  constructor(formBuilder: FormBuilder, router: Router, store: Store<{ order: Order }>) {
    this.formBuilder = formBuilder
    this.router = router
    this.store = store
    this.order$ = store.pipe(select('order'))
  }

  ngOnInit(): void {
    window.scroll(0, 0)
    this.form = this.formBuilder.group({
      events: this.formBuilder.array([
        this.formBuilder.group({
          number: new FormControl('1'),
          address: new FormControl(''),
          description: new FormControl(''),
          toDelete: new FormControl('false'),
        }),

        this.formBuilder.group({
          number: new FormControl('2'),
          address: new FormControl(''),
          description: new FormControl(''),
          toDelete: new FormControl('false'),
        }),
      ])
    })
  }

  onAddEvent() {
    const events: FormArray = (this.form.get('events') as FormArray)
    if (events.length < 10) {
      events.push(
        this.formBuilder.group({
          number: new FormControl(events.length + 1),
          address: new FormControl(''),
          description: new FormControl(''),
          toDelete: new FormControl('false'),
        })
      )
    }
  }

  onDeleteEvent(idx: number) {
    const events: FormArray = (this.form.get('events') as FormArray)
    if (events.length > 2) {
      events.removeAt(idx)
    }

    events.controls.forEach((it, idx) => {
      it.get('number').setValue(idx + 1)
    })
  }

  onSubmit() {
    if ( this.form.valid ) {
      this.store.dispatch(addOrder({ order: new Order() }))
    }

    this.router.navigateByUrl('/received')
  }
}
