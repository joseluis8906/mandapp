import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  items: HistoryItem[]
  router: Router

  constructor(router: Router) {
    this.router = router
    this.items = [
      new HistoryItem('09:30 am', 3, 7500, 'created'),
      new HistoryItem('10:15 am', 2, 5200, 'in progress'),
      new HistoryItem('11:41 am', 3, 8600, 'completed'),
      new HistoryItem('11:41 am', 4, 11300, 'uncompleted'),
      new HistoryItem('11:41 am', 2, 6400, 'canceled')
    ]
  }

  ngOnInit(): void {
    window.scroll(0, 0)
  }

  goToCreate(): void {
    this.router.navigateByUrl('/create')
  }

}

class HistoryItem {
  hour: string
  events: number
  amount: number
  state: string 

  constructor(hour: string, events: number, amount: number, state: string) {
    this.hour = hour
    this.events = events
    this.amount = amount
    this.state = state
  }
}
