import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {
  score: number

  constructor() { }

  ngOnInit(): void {
  }

  onScore(score: number) {
    this.score = score
  }

}
