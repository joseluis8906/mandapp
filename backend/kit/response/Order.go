package response

import (
	"encoding/json"
	"mandapp/kit/vo"
)

type Order struct {
	Id vo.UUID `json:"id"`
}

func (t Order) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}
