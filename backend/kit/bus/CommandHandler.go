package bus

import "mandapp/kit/command"

type CommandHandler interface {
	Perform(command command.Command)
}
