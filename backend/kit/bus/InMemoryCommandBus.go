package bus

import (
	"mandapp/kit/command"
	"sync"
)

var (
	inMemoryCommandBusInstance *InMemoryCommandBus
	inMemoryCommandBusOnce     sync.Once
)

type InMemoryCommandBus struct {
	store map[string]CommandHandler
}

func (T InMemoryCommandBus) Unique() *InMemoryCommandBus {
	inMemoryCommandBusOnce.Do(func() {
		inMemoryCommandBusInstance = &InMemoryCommandBus{
			store: make(map[string]CommandHandler),
		}
	})

	return inMemoryCommandBusInstance
}

func (T *InMemoryCommandBus) Dispatch(cmd command.Command) {
	if _, ok := T.store[cmd.ID()]; ok {
		T.store[cmd.ID()].Perform(cmd)
	}
}

func (T *InMemoryCommandBus) Register(cmdId string, handler CommandHandler) {
	T.store[cmdId] = handler
}
