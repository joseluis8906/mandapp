package bus

import (
	"mandapp/kit/command"
	"mandapp/kit/response"
)

type QueryBus interface {
	Dispatch(command command.Command) response.Response
	Register(commandId string, handler QueryHandler)
}
