package bus

import (
	"mandapp/kit/command"
	"mandapp/kit/response"
)

type QueryHandler interface {
	Perform(command command.Command) response.Response
}
