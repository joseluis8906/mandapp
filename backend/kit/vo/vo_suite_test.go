package vo_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestValueObjects(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Vo Suite")
}
