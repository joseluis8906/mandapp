package vo

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type Phone struct {
	string
}

func (T Phone) Default() Phone {
	T.string = ""
	return T
}

func (T Phone) From(raw string) Phone {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Phone) String() string {
	return T.string
}

func (T *Phone) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	return errors.New(fmt.Sprintf("value doesn't match Phone: %s", T.String()))
}

func (T Phone) IsZero() bool {
	var re = regexp.MustCompile(`([+])*([0-9]{1,2})*([0-9]{3})+([0-9]{3})+([0-9]{4})+`)

	return re.MatchString(T.string)
}
