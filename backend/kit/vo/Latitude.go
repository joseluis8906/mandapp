package vo

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math"
)

type Latitude struct {
	float32
}

func (T Latitude) Default() Latitude {
	T.float32 = 0.0
	return T
}

func (T Latitude) From(raw float32) Latitude {
	T.float32 = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Latitude) String() string {
	return fmt.Sprintf("%.12f", T.float32)
}

func (T *Latitude) UnmarshalJSON(b []byte) error {
	bits := binary.LittleEndian.Uint32(b)
	value := math.Float32frombits(bits)

	T.float32 = value

	if !T.IsZero() {
		return nil
	}

	return errors.New(fmt.Sprintf("value doesn't match Currency: %s", T.String()))
}

func (T Latitude) IsZero() bool {
	return T.float32 == 0.0
}
