package vo_test

import (
	"fmt"
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"
)

var _ = Describe("Phone", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument.", func() {
			phoneFake := fmt.Sprintf(
				"+%s%s%s%s",
				faker.Number().Number(2),
				faker.Number().Number(3),
				faker.Number().Number(3),
				faker.Number().Number(4))
			res := vo.Phone{}.From(phoneFake)

			Expect(res).Should(Equal(vo.Phone{}.From(phoneFake)))
		})
	})
})
