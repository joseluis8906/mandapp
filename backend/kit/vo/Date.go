package vo

import (
	"errors"
	"fmt"
	"strconv"
	"time"
)

type Date struct {
	time.Time
}

func (T Date) Default() Date {
	T.Time = T.AddDate(-101, 1, 1)
	return T
}

func (T Date) From(raw time.Time) Date {
	instance := Date{Time: raw}

	if !instance.IsZero() {
		return instance
	}

	return T.Default()
}

func(T Date) IsZero() bool {
	lowLimit := T.Year() >= time.Now().Year() - 100
	highLimit := T.Year() <= time.Now().Year() + 100

	return !(lowLimit && highLimit)
}

func (T *Date) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(T.String())), nil
}

func (T *Date) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	date, err := time.Parse("2006-01-02 15:04:05", value)
	if err != nil {
		return err
	}

	T.Time = date

	if T.IsZero() {
		return nil
	}

	T.Time = time.Date(1920, 1, 1, 0, 0, 0, 0, nil)

	return errors.New(fmt.Sprintf("value doesn't match Currency: %s", T.String()))
}

func(T *Date) String() string {
	return T.Format("2006-01-02 15:04:05")
}
