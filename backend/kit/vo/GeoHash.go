package vo

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type GeoHash struct {
	string
}

func (T GeoHash) Default() GeoHash {
	T.string = ""
	return T
}

func (T GeoHash) From(raw string) GeoHash {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T GeoHash) String() string {
	return T.string
}

func (T *GeoHash) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	return errors.New(fmt.Sprintf("value doesn't match GeoHash: %s", T.String()))
}

func (T GeoHash) IsZero() bool {
	var re = regexp.MustCompile(`(d6|d3|d2|6r)(\w){3,8}`)

	return !re.MatchString(T.String())
}
