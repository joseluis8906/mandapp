package vo

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type Address struct {
	string
}

func (T Address) Default() Address {
	T.string = ""

	return T
}

func (T Address) From(raw string) Address {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Address) String() string {
	return T.string
}

func (T *Address) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	T.string = ""

	return errors.New(fmt.Sprintf("value doesn't match Address: %s", value))
}

func (T Address) IsZero() bool {
	var re = regexp.MustCompile(
		`(cra|carrera|calle|cll|diag|diagonal|transversal|tra)\s*([0-9]){1,3}\s*#\s*([0-9]){1,3}([a-zA-Z])?\s*-*\s*([0-9]){1,3}`,
	)

	return !re.MatchString(T.string)
}
