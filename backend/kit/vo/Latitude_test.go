package vo_test

import (
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"
)

var _ = Describe("Latitude", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument.", func() {
			latFake := faker.Address().Latitude()
			res := vo.Latitude{}.From(latFake)

			Expect(res).Should(Equal(vo.Latitude{}.From(latFake)))
		})
	})
})
