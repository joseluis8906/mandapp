package vo

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type Country struct {
	string
}

func (T Country) Default() Country {
	T.string = ""
	return T
}

func (T Country) From(raw string) Country {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Country) String() string {
	return T.string
}

func (T *Country) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if T.IsZero() {
		return nil
	}

	T.string = ""

	return errors.New(fmt.Sprintf("value doesn't match Country: %s", T.String()))
}

func (T Country) IsZero() bool {
	var re = regexp.MustCompile(
		`([a-zA-Z]{2,16})\s*([a-zA-Z]{2,16})\s*([a-zA-Z]{2,16})*`,
	)

	return !re.MatchString(T.String())
}
