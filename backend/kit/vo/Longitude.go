package vo

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math"
)

type Longitude struct {
	float32
}

func (T Longitude) Default() Longitude {
	T.float32 = 0.0
	return T
}

func (T Longitude) From(raw float32) Longitude {
	T.float32 = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Longitude) String() string {
	return fmt.Sprintf("%.12f", T.float32)
}

func (T *Longitude) UnmarshalJSON(b []byte) error {
	bits := binary.LittleEndian.Uint32(b)
	value := math.Float32frombits(bits)

	T.float32 = value

	if !T.IsZero() {
		return nil
	}

	return errors.New(fmt.Sprintf("value doesn't match Longitude: %s", T.String()))
}

func (T Longitude) IsZero() bool {
	return T.float32 == 0.0
}
