package vo_test

import (
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"
)

var _ = Describe("Longitude", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument.", func() {
			lngFake := faker.Address().Longitude()
			res := vo.Longitude{}.From(lngFake)

			Expect(res).Should(Equal(vo.Longitude{}.From(lngFake)))
		})
	})
})
