package vo

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/google/uuid"
)

type UUID struct {
	uuid.UUID
}

func (T UUID) Default() UUID {
	T = T.Parse("00000000-0000-0000-0000-000000000000")
	return T
}

func (T UUID) Random() UUID {
	T.UUID, _ = uuid.NewRandom()
	return T
}

func (T UUID) From(raw uuid.UUID) UUID {
	T.UUID = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T UUID) String() string {
	return T.UUID.String()
}

func (T *UUID) MarshalJSON() ([]byte, error) {
	return []byte(strconv.Quote(T.String())), nil
}

func (T *UUID) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	uid, err := uuid.Parse(value)

	T.UUID = uid

	if !T.IsZero() {
		return nil
	}

	return errors.New(fmt.Sprintf("value doesn't match UUID: %s", T.String()))
}

func (T UUID) IsZero() bool {
	return T.String() == "00000000-0000-0000-0000-000000000000"
}

func (T UUID) Parse(value string) UUID {
	raw, err := uuid.Parse(value)
	if err != nil {
		return T.Default()
	}

	T.UUID = raw
	return T
}
