package vo_test

import (
	"encoding/json"
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("Currency", func() {
	Context("empty context", func() {
		It("should parse json to currency", func() {
			input := []byte(`{
				"amount": 12000
			}`)

			cur := struct {
				Amount vo.Currency `json:"amount"`
			}{Amount: vo.Currency{}.Default()}
			err := json.Unmarshal(input, &cur)

			Expect(err).Should(BeNil())
			Expect(cur.Amount).Should(Equal(vo.Currency{}.From(12000)))
		})
	})
})
