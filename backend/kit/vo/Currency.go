package vo

import (
	"errors"
	"fmt"
	"strconv"
)

type Currency struct {
	float64
}

func (T Currency) Default() Currency {
	T.float64 = 0.0
	return T
}

func (T Currency) From(raw float64) Currency {
	T.float64 = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T Currency) String() string {
	return fmt.Sprintf("%.10f", T.float64)
}

func (T *Currency) UnmarshalJSON(b []byte) error {
	value, _ := strconv.ParseFloat(string(b), 64)

	T.float64 = value

	if T.IsZero() {
		return errors.New(fmt.Sprintf("value doesn't match Currency: %s", T.String()))
	}

	return nil
}

func (T Currency) IsZero() bool {
	return T.float64 < 0
}
