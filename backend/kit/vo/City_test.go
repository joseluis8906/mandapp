package vo_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"mandapp/kit/vo"
	"syreclabs.com/go/faker"
)

var _ = Describe("City", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument.", func() {
			cityFake := faker.Address().City()
			res := vo.City{}.From(cityFake)

			Expect(cityFake).Should(Equal(res.String()))
		})
	})
})
