package vo

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type City struct {
	string
}

func (T City) Default() City {
	T.string = ""
	return T
}

func (T City) From(raw string) City {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T City) String() string {
	return T.string
}

func (T *City) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	T.string = ""

	return errors.New(fmt.Sprintf("value doesn't match City: %s", T.String()))
}

func (T City) IsZero() bool {
	var re = regexp.MustCompile(`([a-zA-Z]{2,16})\s*([a-zA-Z]{2,16})\s*([a-zA-Z]{2,16})`)

	return !re.MatchString(T.string)
}
