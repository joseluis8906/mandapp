package command

import (
	"encoding/json"
	"mandapp/internal/order"
	"mandapp/kit/vo"
)

type FinishOrder struct {
	OrderId vo.UUID          `json:"order_id"`
	State   order.OrderState `json:"state"`
}

func (t FinishOrder) ID() string {
	return "[Order] FinishCommand"
}

func (t FinishOrder) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}
