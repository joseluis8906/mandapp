package command

type Command interface {
	ID() string
	Data() []byte
}
