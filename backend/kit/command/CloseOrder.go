package command

import (
	"encoding/json"
	"mandapp/internal/order"
	"mandapp/kit/vo"
)

type CloseOrder struct {
	OrderId vo.UUID          `json:"order_id"`
	Score   order.OrderScore `json:"score"`
}

func (t CloseOrder) ID() string {
	return "[Order] CloseCommand"
}

func (t CloseOrder) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}
