package command

import (
	"encoding/json"
	"mandapp/kit/vo"
)

type TakeOrder struct {
	OrderId   vo.UUID `json:"order_id"`
	CourierId vo.UUID `json:"courier_id"`
}

func (T TakeOrder) ID() string {
	return "[Order] TakeCommand"
}

func (t TakeOrder) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}
