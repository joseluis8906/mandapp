package validation

type Validator interface {
	IsZero() bool
}
