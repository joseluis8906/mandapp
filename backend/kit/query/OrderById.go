package query

import (
	"encoding/json"
	"mandapp/kit/vo"
)

type OrderById struct {
	OrderId vo.UUID `json:"order_id"`
}

func (t OrderById) ID() string {
	return "[Order] ByIdQuery"
}

func (t OrderById) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}
