package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/fvbock/endless"
	"github.com/gin-gonic/gin"
)

var appOnce sync.Once
var appInstance *App

type App struct {
	router *gin.Engine
}

func (t App) Unique() *App {
	appOnce.Do(func() {
		if appInstance == nil {
			appInstance = &App{
				router: gin.Default(),
			}
		}
	})

	return appInstance
}

func (T *App) Register(method string, endpoint string, handlers ...gin.HandlerFunc) {
	switch method {
	case http.MethodPut:
		T.router.PUT(endpoint, handlers...)
		break
	case http.MethodGet:
		T.router.GET(endpoint, handlers...)
		break
	case http.MethodPatch:
		T.router.PATCH(endpoint, handlers...)
		break
	case http.MethodDelete:
		T.router.DELETE(endpoint, handlers...)
		break
	default:
		log.Println("method not allowed")
	}
}

func (t *App) IsValid() bool {
	return t.router != nil
}

func (t *App) Start() {
	err := endless.ListenAndServe("localhost", t.router)
	if err != nil {
		log.Fatal(err)
	}

	t.stop()
}

func (t *App) stop() {
	fmt.Println(">>>>>> App stopped...")
}
