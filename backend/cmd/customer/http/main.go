package main

func main() {
	SetUp()
	app = App{}.Unique()
	defer app.stop()
	app.Start()
}
