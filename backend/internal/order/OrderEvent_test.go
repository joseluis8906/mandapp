package order_test

import (
	"encoding/json"
	"mandapp/internal/order"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("OrderEvent", func() {
	Context("empty context", func() {
		It("should parse json to events", func() {
			input := []byte(`[
				{
					"number": 1,
					"address": "cra 1 # 2 - 3",
					"description": "default 1"
				},
				{
					"number": 2,
					"address": "cra 2 # 3 - 4",
					"description": "default 2"
				}
			]`)
			evts := order.OrderEventCollection{}.Default()
			err := json.Unmarshal(input, &evts)

			Expect(err).To(BeNil())
			Expect(evts.Size()).To(Equal(2))
		})
	})
})
