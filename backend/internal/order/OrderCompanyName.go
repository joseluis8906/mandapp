package order

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
)

type OrderCompanyName struct {
	string
}

func (T OrderCompanyName) Default() OrderCompanyName {
	T.string = ""
	return T
}

func (T OrderCompanyName) From(raw string) OrderCompanyName {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T OrderCompanyName) String() string {
	return T.string
}

func (T *OrderCompanyName) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	T.string = ""

	return errors.New(fmt.Sprintf("value doesn't match OrderCompanyName: %s", T.String()))
}

func (T *OrderCompanyName) IsZero() bool {
	var re = regexp.MustCompile(`(([a-zA-Z]{3,16})\s*){1,6}(s.a.s|s.a|ltda|cia & [a-zA-Z]{2,8})+`)

	return !re.MatchString(T.string)
}
