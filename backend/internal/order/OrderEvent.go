package order

import (
	"encoding/json"
	"errors"
	"fmt"
	"mandapp/kit/vo"
)

type OrderEvent struct {
	Number      int        `json:"number"`
	Address     vo.Address `json:"address"`
	Description string     `json:"description"`
}

func (T OrderEvent) Default() OrderEvent {
	T.Number = 0
	T.Address = vo.Address{}.Default()
	T.Description = ""

	return T
}

func (T *OrderEvent) IsZero() bool {
	return !(T.Number > 0 && !T.Address.IsZero() && T.Description != "")
}

func (T *OrderEvent) UnmarshalJSON(b []byte) error {
	type Alias OrderEvent
	aux := Alias{}
	err := json.Unmarshal(b, &aux)
	if err != nil {
		return err
	}

	*T = OrderEvent(aux)

	if !T.IsZero() {
		return nil
	}

	*T = T.Default()

	return errors.New(fmt.Sprintf("value doesn't match OrderEvent: %s", T.String()))
}

func (T *OrderEvent) String() string {
	res, _ := json.Marshal(T)
	return string(res[:])
}

// OrderEventCollection
type OrderEventCollection struct {
	data []OrderEvent
}

func (T OrderEventCollection) Default() OrderEventCollection {
	return OrderEventCollection{
		data: make([]OrderEvent, 0),
	}
}

func (T OrderEventCollection) From(raw []OrderEvent) OrderEventCollection {
	return OrderEventCollection{
		data: raw,
	}
}

func (T OrderEventCollection) IsZero() bool {
	return T.Size() == 0
}

func (T *OrderEventCollection) Add(el OrderEvent) {
	T.data = append(T.data, el)
}

func (T OrderEventCollection) Map(cb func(elm OrderEvent) OrderEvent) OrderEventCollection {
	res := OrderEventCollection{}.Default()

	for _, val := range T.data {
		res.Add(cb(val))
	}

	return res
}

func (T *OrderEventCollection) ForEach(cb func(elm OrderEvent)) {
	for _, val := range T.data {
		cb(val)
	}
}

func (T OrderEventCollection) Size() int {
	if T.data != nil {
		return len(T.data)
	}

	return 0
}

func (T *OrderEventCollection) UnmarshalJSON(b []byte) error {
	aux := make([]OrderEvent, 0)
	err := json.Unmarshal(b, &aux)
	if err != nil {
		return err
	}

	T.data = aux

	if !T.IsZero() {
		return nil
	}

	*T = T.Default()

	return errors.New(fmt.Sprintf("value doesn't match OrderEventCollection: %v", aux))
}
