package order

import (
	"errors"
	"fmt"
	"strconv"
)

type OrderScore struct {
	int
}

func (T OrderScore) Default() OrderScore {
	T.int = 0
	return T
}

func (T OrderScore) From(raw int) OrderScore {
	T.int = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T OrderScore) IsZero() bool {
	return !(T.int >= 1 && T.int <= 5)
}

func (T *OrderScore) UnmarshalJSON(b []byte) error {
	value, err := strconv.Atoi(string(b))

	if err != nil {
		return err
	}

	T.int = value

	if T.IsZero() {
		return nil
	}

	T.int = 0

	return errors.New(fmt.Sprintf("value doesn't match Order Score: %d", T.int))
}

func (T OrderScore) String() string {
	return fmt.Sprintf("%d", T.int)
}
