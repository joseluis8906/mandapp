package order

import "mandapp/kit/vo"

type OrderRepository interface {
	Empty() Order
	Save(order Order) error
	Update(order Order) error
	GetById(id vo.UUID) (Order, error)
}
