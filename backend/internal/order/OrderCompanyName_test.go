package order_test

import (
	"fmt"
	"mandapp/internal/order"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"
)

var _ = Describe("OrderCompanyName", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument.", func() {
			nameFake := fmt.Sprintf("%s %s",
				faker.Name(),
				faker.RandomChoice([]string{"s.a", "ltda", "s.a.s", "cia & co"}))
			res := order.OrderCompanyName{}.From(nameFake)

			Expect(nameFake).Should(Equal(res.String()))
		})
	})
})
