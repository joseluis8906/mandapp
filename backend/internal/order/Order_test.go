package order_test

import (
	"mandapp/internal/order"
	"mandapp/kit/vo"

	"syreclabs.com/go/faker"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("OrderEntity", func() {

	Context("given the intention to request an order", func() {
		When("an arrival event is triggered", func() {
			It("should set order state to created", func() {
				newOrder := order.Order{}.Default()
				newOrder.Arrive(
					vo.UUID{}.Random(),
					vo.UUID{}.Random(),
					order.OrderEventCollection{}.Default(),
					vo.Currency{}.From(float64(faker.RandomInt(1000, 1000))),
				)

				Expect(newOrder.IsCreated()).To(BeTrue())
			})
		})
	})

})
