package order

import (
	"errors"
	"fmt"
	"strconv"
)

var (
	OrderStateCreated     = "created"
	OrderStateInProgress  = "in progress"
	OrderStateCompleted   = "completed"
	OrderStateUncompleted = "uncompleted"
	OrderStateCanceled    = "canceled"
)

type OrderState struct {
	string
}

func (T OrderState) Default() OrderState {
	T.string = ""
	return T
}

func (T OrderState) From(raw string) OrderState {
	T.string = raw

	if !T.IsZero() {
		return T
	}

	return T.Default()
}

func (T OrderState) String() string {
	return T.string
}

func (T *OrderState) UnmarshalJSON(b []byte) error {
	value, err := strconv.Unquote(string(b))

	if err != nil {
		return err
	}

	T.string = value

	if !T.IsZero() {
		return nil
	}

	T.string = ""

	return errors.New(fmt.Sprintf("value doesn't match Order State: %s", T.String()))
}

func (T OrderState) IsZero() bool {
	switch T.string {
	case OrderStateCreated, OrderStateInProgress, OrderStateCompleted, OrderStateUncompleted, OrderStateCanceled:
		return false
	default:
		return true
	}
}
