package order

import (
	"errors"
	"fmt"
	"mandapp/kit/vo"
	"time"
)

type Order struct {
	id         vo.UUID
	customerId vo.UUID
	courierId  vo.UUID
	events     OrderEventCollection
	amount     vo.Currency
	state      OrderState
	score      OrderScore
	createdAt  vo.Date
	updatedAt  vo.Date
}

func (T Order) Default() Order {
	return Order{}
}

func (T Order) Id() vo.UUID {
	return T.id
}

func (T Order) IsZero() bool {
	if T.Id().Time() == 0 {
		return true
	} else {
		return false
	}
}

func (T Order) Cmp(another Order) int {
	if T.id.Time() == another.id.Time() {
		return 0
	} else if T.id.Time() < another.id.Time() {
		return -1
	} else {
		return 1
	}
}

func (T *Order) Arrive(
	orderId vo.UUID,
	customerId vo.UUID,
	events OrderEventCollection,
	amount vo.Currency) {

	T.id = orderId
	T.customerId = customerId
	T.events = events
	T.amount = amount
	T.state = OrderState{}.From(OrderStateCreated)
	T.score = OrderScore{}.Default()
	T.createdAt = vo.Date{}.From(time.Now())
	T.updatedAt = vo.Date{}.From(time.Now())
}

func (T *Order) Taken(courierId vo.UUID) error {
	if T.state.String() == OrderStateCreated {
		T.courierId = courierId
		T.state = OrderState{}.From(OrderStateInProgress)
		T.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be taken because was not arrived", T.id.String()))
}

func (T *Order) Completed() error {
	if T.state.String() == OrderStateInProgress {
		T.state = OrderState{}.From(OrderStateCompleted)
		T.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be complete because is not in progress",
		T.id.String()))
}

func (T *Order) Uncompleted() error {
	if T.state.String() == OrderStateInProgress {
		T.state = OrderState{}.From(OrderStateUncompleted)
		T.updatedAt = vo.Date{}.From(time.Now())
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be uncomplete because is not in progress",
		T.id.String()))
}

func (T *Order) Canceled() error {
	isCreated := T.state.String() == OrderStateCreated
	isInProgress := T.state.String() == OrderStateInProgress
	if isCreated || isInProgress {
		T.state = OrderState{}.From(OrderStateCanceled)
		T.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be canceled because is not arrived or in progress",
		T.id.String()))
}

func (T *Order) Close(score OrderScore) error {
	isCompleted := T.state.String() == OrderStateCompleted
	isUncomplete := T.state.String() == OrderStateUncompleted
	isValid := isCompleted || isUncomplete
	if T.score.IsZero() && isValid {
		T.score = score
		T.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be canceled because is not complete or uncomplete",
		T.id.String()))
}

func (T Order) IsCreated() bool {
	return T.state.String() == OrderStateCreated
}

func (T Order) IsInProgress() bool {
	return T.state.String() == OrderStateInProgress
}

func (T Order) IsCompleted() bool {
	return T.state.String() == OrderStateCompleted
}

func (T Order) IsUncompleted() bool {
	return T.state.String() == OrderStateUncompleted
}

func (T Order) IsCanceled() bool {
	return T.state.String() == OrderStateCanceled
}
