package OrdDom

import (
	"errors"
	"fmt"
	"mandapp/internal/Customer/Shared/CustDomain"
	"time"
)

type Order struct {
	id         vo.UUID
	customerId vo.UUID
	courierId  vo.UUID
	events     OrderEventCollection
	amount     vo.Currency
	state      OrderState
	score      OrderScore
	createdAt  vo.Date
	updatedAt  vo.Date
}

func (t Order) Id() vo.UUID {
	return t.id
}

func (t Order) IsZero() bool {
	if t.Id().Time() == 0 {
		return true
	} else {
		return false
	}
}

func (t Order) Cmp(another Order) int {
	if t.id.Time() == another.id.Time() {
		return 0
	} else if t.id.Time() < another.id.Time() {
		return -1
	} else {
		return 1
	}
}

func (t *Order) Arrive(
	orderId vo.UUID,
	customerId vo.UUID,
	events OrderEventCollection,
	amount vo.Currency) {

	t.id = orderId
	t.customerId = customerId
	t.events = events
	t.amount = amount
	t.state = OrderStateCreated
	t.score, _ = OrderScore{}.Default()
	t.createdAt, _ = vo.Date{}.From(time.Now())
	t.updatedAt, _ = vo.Date{}.From(time.Now())
}

func (t *Order) Taken(courierId vo.UUID) error {
	if t.state == OrderStateCreated {
		t.courierId = courierId
		t.state = OrderStateInProgress
		t.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be taken because was not arrived", t.id.String()))
}

func (t *Order) Completed() error {
	if t.state == OrderStateInProgress {
		t.state = OrderState{}.From(OrderStateCompleted.String())
		t.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be complete because is not in progress", t.id.String()))
}

func (t *Order) Uncompleted() error {
	if t.state == OrderStateInProgress {
		t.state = OrderState{}.From(OrderStateUncompleted.String())
		t.updatedAt = CustDomain.NewDate(time.Now())
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be uncomplete because is not in progress", t.id.String()))
}

func (t *Order) Canceled() error {
	if t.state == OrderStateCreated || t.state == OrderStateInProgress {
		t.state = OrderState{}.From(OrderStateCanceled.String())
		t.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be canceled because is not arrived or in progress", t.id.String()))
}

func (t *Order) Close(score OrderScore) error {
	stateIsValid := t.state == OrderStateCompleted || t.state == OrderStateUncompleted
	if t.score.IsZero() && stateIsValid {
		t.score = score
		t.updatedAt = vo.Date{}.From(time.Now())

		return nil
	}

	return errors.New(fmt.Sprintf(
		"order %s can't be canceled because is not complete or uncomplete", t.id.String()))
}
