package order

import (
	"mandapp/kit/vo"
	"sync"
)

var once sync.Once
var inMemoryRepositoryInstance *InMemoryRepository

func (T InMemoryRepository) Unique() *InMemoryRepository {
	once.Do(func() {
		inMemoryRepositoryInstance = &InMemoryRepository{
			data: OrderCollection{},
		}
	})

	return inMemoryRepositoryInstance
}

type InMemoryRepository struct {
	data OrderCollection
}

func (T InMemoryRepository) CreateEmpty() Order {
	return Order{}
}

func (T *InMemoryRepository) Save(od Order) error {
	T.data.Add(od)

	return nil
}

func (T *InMemoryRepository) Update(od Order) error {
	T.data = T.data.Map(func(it Order) Order {
		if it.Cmp(od) == 0 {
			return od
		}

		return it
	})

	return nil
}

func (T *InMemoryRepository) GetById(id vo.UUID) (Order, error) {
	res := T.data.Filter(func(it Order) bool {
		return it.Id().Time() == id.Time()
	})

	if len(res.data) == 0 {
		return Order{}, nil
	} else if len(res.data) > 1 {
		return Order{}, nil
	}

	return res.data[0], nil
}
