package OrdInf

import (
	"mandapp/internal/Customer/Order/OrdDom"
	skDom "mandapp/internal/Customer/Shared/CustDomain"
	"sync"
)

var once sync.Once
var inMemoryRepositoryInstance *InMemoryRepository

func NewInMemoryRepository() *InMemoryRepository {
	once.Do(func() {
		inMemoryRepositoryInstance = &InMemoryRepository{
			orders: make(OrdDom.OrderCollection, 0),
		}
	})

	return inMemoryRepositoryInstance
}

type InMemoryRepository struct{
	orders OrdDom.OrderCollection
}

func (T *InMemoryRepository) CreateEmpty() OrdDom.Order {
	return OrdDom.Order{}
}

func (T *InMemoryRepository) Save(od OrdDom.Order) error {
	T.orders.Add(od)

	return nil
}

func (T *InMemoryRepository) Update(od OrdDom.Order) error {
	for idx, _ := range T.orders {
		if T.orders[idx].Cmp(od) == 0 {
			T.orders[idx] = od
			break
		}
	}

	return nil
}

func (T *InMemoryRepository) GetById(id skDom.UUID) (OrdDom.Order, error) {
	res := T.orders.Filter(func(it OrdDom.Order) bool {
		return it.Id().Time() == id.Time()
	})

	if len(res) == 0 {
		return OrdDom.Order{}, nil
	} else if len(res) > 1 {
		return OrdDom.Order{}, nil
	}

	return res[0], nil
}
