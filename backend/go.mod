module mandapp

go 1.13

require (
	github.com/Microsoft/hcsshim v0.8.7 // indirect
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/containerd/continuity v0.0.0-20200228182428-0f16d7a0959c // indirect
	github.com/docker/go-connections v0.4.0
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/fluent/fluent-logger-golang v1.4.0
	github.com/fvbock/endless v0.0.0-20170109170031-447134032cb6
	github.com/gin-gonic/gin v1.5.0
	github.com/go-playground/locales v0.12.1
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gocql/gocql v0.0.0-20200410100145-b454769479c6
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang-collections/go-datastructures v0.0.0-20150211160725-59788d5eb259
	github.com/golang/mock v1.4.0
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/golobby/container v1.2.3
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/klauspost/compress v1.10.3 // indirect
	github.com/onsi/ginkgo v1.10.1
	github.com/onsi/gomega v1.8.1
	github.com/pelletier/go-toml v1.4.0
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/segmentio/kafka-go v0.3.4
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/testcontainers/testcontainers-go v0.3.1
	github.com/tinylib/msgp v1.1.1 // indirect
	github.com/xakep666/mongo-migrate v0.2.1
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d // indirect
	google.golang.org/appengine v1.4.0
	google.golang.org/genproto v0.0.0-20200401122417-09ab7b7031d2 // indirect
	google.golang.org/grpc v1.28.0 // indirect
	syreclabs.com/go/faker v1.2.0
)
